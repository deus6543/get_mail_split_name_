check = false;

function add_cacallbacks_template() {

    $("#getmail_smtp_widget").on("click", function() {
        if (!check) {
            name = $("input[name='contact[NAME]']").attr('value');
            //console.log("#getmail_smtp_widget");
            temp = setInterval(function() {
                if ($("#getmail_smtp_widget").find(".template").length > 0) {
                    clearInterval(temp);
                    $("#getmail_smtp_widget").find(".template").on("click", function(event) {
                        temp = setInterval(function() {
                            email_modal = $('#getmail_smtp_widget_email_modal').find("iframe").contents().find("html > body")
                            if (email_modal.html()) {
                                clearInterval(temp);
                                // console.log($('#getmail_smtp_widget_email_modal').find("iframe > html > body"));

                                //console.log(name);
                                //console.log(email_modal);
                                text_name_pos = email_modal.html().indexOf(name);
                                split_name = name.split(" ");
                                if (text_name_pos != -1 && split_name.length == 3) {
                                    length_name = name.length;
                                    //console.log(text.substr(text_name_pos, length_name));
                                    text_start = email_modal.html().substr(0, text_name_pos);
                                    //console.log(text_start);
                                    text_end = email_modal.html().substr(text_start.length + length_name);
                                    //console.log(text_end);

                                    return_text = text_start + split_name[1] + text_end;
                                    //console.log(return_text);
                                    email_modal.html(return_text);
                                }
                            }
                        }, 500);

                    })
                }
            }, 1500);
            check = true;
        }
    });


}

define(['jquery'], function($) {
    var CustomWidget = function() {
        var self = this;
        this.callbacks = {
            render: function() {

                return true;
            },
            init: function() {

                //add_cacallbacks_template();
                return true;
            },
            bind_actions: function() {

                // console.log('bind_');
                var temp2 = setInterval(function() {
                    if ($("#getmail_smtp_widget").length > 0) {
                        add_cacallbacks_template();
                        clearInterval(temp2);
                    }
                }, 1500);

                return true;
            },
            settings: function() {
                return true;
            },
            onSave: function() {
                //alert('click');
                return true;
            },
            destroy: function() {

            },
            contacts: {
                //select contacts in list and clicked on widget name
                selected: function() {
                    // console.log('contacts');
                }
            },
            leads: {
                //select leads in list and clicked on widget name
                selected: function() {
                    console.log('leads');
                }
            },
            tasks: {
                //select taks in list and clicked on widget name
                selected: function() {
                    // console.log('tasks');
                }
            }
        };
        return this;
    };

    return CustomWidget;
});